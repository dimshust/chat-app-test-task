package com.dimshust.chatapp.service;

import com.dimshust.chatapp.dto.ChatRoomDto;
import com.dimshust.chatapp.main.model.ChatRoom;
import com.dimshust.chatapp.main.repository.ChatRoomRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import static org.junit.Assert.assertEquals;

import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.NoSuchElementException;
import java.util.Optional;

public class ChatRoomServiceImplUnitTest {

    public static final String CHAT_ROOM_NAME = "TEST";

    @InjectMocks
    ChatRoomServiceImpl chatRoomService;

    @Mock
    ChatRoomRepository chatRoomRepositoryMock;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createChatRoomSuccessTest() throws Exception {
        when(chatRoomRepositoryMock.findById(anyString())).thenReturn(Optional.empty());
        when(chatRoomRepositoryMock.save(any(ChatRoom.class))).thenReturn(dummyChatRoom());
        ChatRoomDto chatRoomDto = chatRoomService.createChatRoom(CHAT_ROOM_NAME);
        assertEquals(CHAT_ROOM_NAME, chatRoomDto.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createChatRoomAlreadyExistsTest() throws Exception {
        when(chatRoomRepositoryMock.findById(anyString())).thenReturn(Optional.of(dummyChatRoom()));
        chatRoomService.createChatRoom(CHAT_ROOM_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createChatRoomInvalidNameTest() throws Exception {
        chatRoomService.createChatRoom("");
    }

    @Test
    public void deleteChatRoomSuccessTest() throws Exception {
        ChatRoomDto chatRoomDto = chatRoomService.deleteChatRoom(CHAT_ROOM_NAME);
        assertEquals(CHAT_ROOM_NAME, chatRoomDto.getName());
    }

    @Test(expected = NoSuchElementException.class)
    public void deleteChatRoomNoSuchRoomTest() throws Exception {
        Mockito.doThrow(new EmptyResultDataAccessException(1)).when(chatRoomRepositoryMock).deleteById(anyString());
        chatRoomService.deleteChatRoom(CHAT_ROOM_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteChatRoomInvalidNameTest() throws Exception {
        chatRoomService.deleteChatRoom("");
    }

    private static ChatRoom dummyChatRoom() {
        ChatRoom out = new ChatRoom();
        out.setName(CHAT_ROOM_NAME);
        return out;
    }
}
