package com.dimshust.chatapp.service;

import com.dimshust.chatapp.dto.ChatRoomUserDto;
import com.dimshust.chatapp.main.model.ChatRoom;
import com.dimshust.chatapp.main.model.ChatRoomUser;
import com.dimshust.chatapp.main.repository.ChatRoomRepository;
import com.dimshust.chatapp.main.repository.ChatRoomUserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import static org.junit.Assert.assertEquals;

public class ChatRoomUserServiceImplUnitTest {

    private static final String CHAT_ROOM_NAME = "TEST_ROOM";
    private static final String CLIENT_ID = "123";
    private static final String USER_NAME = "TEST_NAME";

    @InjectMocks
    ChatRoomUserServiceImpl chatRoomUserService;

    @Mock
    ChatRoomRepository chatRoomRepositoryMock;

    @Mock
    ChatRoomUserRepository chatRoomUserRepositoryMock;

    @Test
    public void testRegisterUserInChatRoomSuccess() {
        when(chatRoomRepositoryMock.findById(anyString())).thenReturn(Optional.of(dummyChatRoom()));
        when(chatRoomUserRepositoryMock
                .findByClientIdAndChatRoomName(anyString(), anyString())).thenReturn(Optional.empty());
        when(chatRoomUserRepositoryMock.save(any(ChatRoomUser.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        when(chatRoomUserRepositoryMock.findByChatRoomNameAndName(anyString(), anyString()))
                .thenReturn(Optional.empty());
        ChatRoomUserDto chatRoomUserDto = chatRoomUserService.registerUserInChatRoom(CLIENT_ID, CHAT_ROOM_NAME, USER_NAME);
        assertEquals(CLIENT_ID, chatRoomUserDto.getClientId());
        assertEquals(USER_NAME, chatRoomUserDto.getName());
        assertEquals(CHAT_ROOM_NAME, chatRoomUserDto.getChatRoomName());
    }

    @Test
    public void testRegisterUserInChatRoomUserAlreadyExists() {
        when(chatRoomRepositoryMock.findById(anyString())).thenReturn(Optional.of(dummyChatRoom()));
        when(chatRoomUserRepositoryMock
                .findByClientIdAndChatRoomName(anyString(), anyString())).thenReturn(Optional.of(dummyChatRoomUser()));
        when(chatRoomUserRepositoryMock.save(any(ChatRoomUser.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        when(chatRoomUserRepositoryMock.findByChatRoomNameAndName(anyString(), anyString()))
                .thenReturn(Optional.empty());
        ChatRoomUserDto chatRoomUserDto = chatRoomUserService.registerUserInChatRoom(CLIENT_ID, CHAT_ROOM_NAME, USER_NAME);
        assertEquals(CLIENT_ID, chatRoomUserDto.getClientId());
        assertEquals(USER_NAME, chatRoomUserDto.getName());
        assertEquals(CHAT_ROOM_NAME, chatRoomUserDto.getChatRoomName());
    }

    @Test
    public void testRegisterUserInChatRoomNameAlreadyExists() {
        when(chatRoomRepositoryMock.findById(anyString())).thenReturn(Optional.of(dummyChatRoom()));
        when(chatRoomUserRepositoryMock
                .findByClientIdAndChatRoomName(anyString(), anyString())).thenReturn(Optional.empty());
        when(chatRoomUserRepositoryMock.save(any(ChatRoomUser.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        when(chatRoomUserRepositoryMock.findByChatRoomNameAndName(anyString(), anyString()))
                .thenReturn(Optional.of(dummyChatRoomUser()));
        ChatRoomUserDto chatRoomUserDto = chatRoomUserService.registerUserInChatRoom(CLIENT_ID, CHAT_ROOM_NAME, USER_NAME);
        assertEquals(CLIENT_ID, chatRoomUserDto.getClientId());
        assertNotEquals(USER_NAME, chatRoomUserDto.getName());
        assertEquals(CHAT_ROOM_NAME, chatRoomUserDto.getChatRoomName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterUserInChatRoomInvalidClientId() {
        chatRoomUserService.registerUserInChatRoom("", CHAT_ROOM_NAME, USER_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterUserInChatRoomInvalidChatRoomName() {
        chatRoomUserService.registerUserInChatRoom(CLIENT_ID, "", USER_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterUserInChatRoomInvalidUserName() {
        chatRoomUserService.registerUserInChatRoom(CLIENT_ID, CHAT_ROOM_NAME, "");
    }

    @Test
    public void testDeleteUserFromChatRoomSuccess() {
        ChatRoomUserDto chatRoomUserDto = chatRoomUserService.deleteUserFromChatRoom(CLIENT_ID, CHAT_ROOM_NAME);
        assertEquals(CLIENT_ID, chatRoomUserDto.getClientId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteUserFromChatRoomInvalidClientId() {
        chatRoomUserService.deleteUserFromChatRoom("", CHAT_ROOM_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteUserFromChatRoomInvalidChatRoomName() {
        chatRoomUserService.deleteUserFromChatRoom(CLIENT_ID, "");
    }
    @Test
    public void testDeleteUserSuccess() {
        ChatRoomUserDto chatRoomUserDto = chatRoomUserService.deleteUser(CLIENT_ID);
        assertEquals(CLIENT_ID, chatRoomUserDto.getClientId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteUserInvalidClientId() {
        chatRoomUserService.deleteUser("");
    }

    @Test
    public void testGetUsersInChatRoomSuccess() {
        when(chatRoomUserRepositoryMock.findByChatRoomName(anyString(), any(Pageable.class)))
                .thenReturn(new PageImpl<>(Collections.singletonList(dummyChatRoomUser())));
        Page<ChatRoomUserDto> result = chatRoomUserService.getUsersInChatRoom(CHAT_ROOM_NAME, 0, 100);
        assertEquals(1, result.getTotalElements());
        assertEquals(USER_NAME, result.getContent().get(0).getName());
        assertEquals(CLIENT_ID, result.getContent().get(0).getClientId());
        assertEquals(CHAT_ROOM_NAME, result.getContent().get(0).getChatRoomName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetUsersInChatRoomInvalidName() {
        chatRoomUserService.getUsersInChatRoom("", 0, 0);
    }

    @Test
    public void testGetUserInChatRoomSuccess() {
        when(chatRoomUserRepositoryMock
                .findByClientIdAndChatRoomName(anyString(), anyString())).thenReturn(Optional.of(dummyChatRoomUser()));
        ChatRoomUserDto res = chatRoomUserService.getUserInChatRoom(CHAT_ROOM_NAME, CLIENT_ID);
        assertEquals(CHAT_ROOM_NAME, res.getChatRoomName());
        assertEquals(CLIENT_ID, res.getClientId());
    }

    @Test(expected = NoSuchElementException.class)
    public void testGetUserInChatRoomNoSuchUser() {
        when(chatRoomUserRepositoryMock
                .findByClientIdAndChatRoomName(anyString(), anyString())).thenReturn(Optional.empty());
        chatRoomUserService.getUserInChatRoom(CHAT_ROOM_NAME, CLIENT_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetUserInChatRoomInvalidChatRoomName() {
        chatRoomUserService.getUserInChatRoom("", CLIENT_ID);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testGetUserInChatRoomInvalidClientId() {
        chatRoomUserService.getUserInChatRoom(CHAT_ROOM_NAME, "");
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private static ChatRoom dummyChatRoom() {
        ChatRoom out = new ChatRoom();
        out.setName(CHAT_ROOM_NAME);
        return out;
    }

    private static ChatRoomUser dummyChatRoomUser() {
        ChatRoomUser out = new ChatRoomUser();
        out.setName(USER_NAME);
        out.setClientId(CLIENT_ID);
        out.setChatRoomName(CHAT_ROOM_NAME);
        return out;
    }
}
