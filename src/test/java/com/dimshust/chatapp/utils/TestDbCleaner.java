package com.dimshust.chatapp.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Service
@Transactional
public class TestDbCleaner {

    @Autowired
    EntityManager em;

    public void clearChatRooms() {
        int deletedCount = em.createNativeQuery("DELETE FROM chat_room").executeUpdate();
    }

}
