package com.dimshust.chatapp.utils;

import com.dimshust.chatapp.dto.ChatMessageDto;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;
import java.util.concurrent.SynchronousQueue;

public class TestStompFrameHandler implements StompFrameHandler {

    private final SynchronousQueue<Object> messageResultQueue;

    public TestStompFrameHandler(SynchronousQueue<Object> messageResultQueue) {
        this.messageResultQueue = messageResultQueue;
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return ChatMessageDto.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        try {
            messageResultQueue.put(payload);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}