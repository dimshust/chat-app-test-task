package com.dimshust.chatapp.utils;

import com.dimshust.chatapp.dto.ChatRoomDto;
import com.dimshust.chatapp.dto.PageDto;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TestRestApiClient implements ApplicationListener<ServletWebServerInitializedEvent> {

    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    public ResponseEntity<ChatRoomDto> createTestRoom(String name) {
        return restTemplate.exchange(createRestUrlWithPort("/api/v1/administrator/chatroom/create?name=" + name)
                , HttpMethod.POST, new HttpEntity<String>(null, new HttpHeaders()), ChatRoomDto.class);
    }

    public ResponseEntity<ChatRoomDto> deleteTestRoom(String name) {
        return restTemplate.exchange(createRestUrlWithPort("/api/v1/administrator/chatroom/delete?name=" + name)
                , HttpMethod.GET, new HttpEntity<String>(null, headers), ChatRoomDto.class);
    }

    public ResponseEntity<PageDto> listRoom() {
        return restTemplate.exchange(createRestUrlWithPort("/api/v1/user/chatroom/list")
                , HttpMethod.GET, new HttpEntity<String>(null, headers), PageDto.class);
    }

    public ResponseEntity<PageDto> usersInChatRoom(String chatRoomName) {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        return restTemplate.exchange(createRestUrlWithPort("/api/v1/user//list?chatRoomName=" + chatRoomName)
                , HttpMethod.GET, entity, PageDto.class);

    }
    private String createRestUrlWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    @Override
    public void onApplicationEvent(ServletWebServerInitializedEvent event) {
        port = event.getWebServer().getPort();
    }
}
