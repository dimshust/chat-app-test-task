package com.dimshust.chatapp.service;

import com.dimshust.chatapp.dto.ChatRoomDto;
import com.dimshust.chatapp.dto.PageDto;
import com.dimshust.chatapp.main.model.ChatRoom;
import com.dimshust.chatapp.main.repository.ChatRoomRepository;
import com.google.common.base.Strings;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
@Transactional
public class ChatRoomServiceImpl implements ChatRoomService {

    private final ChatRoomRepository chatRoomRepository;

    public ChatRoomServiceImpl(ChatRoomRepository chatRoomRepository) {
        this.chatRoomRepository = chatRoomRepository;
    }

    @Override
    public ChatRoomDto createChatRoom(String name) {
        if(Strings.isNullOrEmpty(name)){
            throw new IllegalArgumentException("Name is empty or null");
        }
        if(chatRoomRepository.findById(name).isPresent()) {
            throw new IllegalArgumentException("chat room with name: " + name + " already exists");
        }
        ChatRoom chatRoom = new ChatRoom();
        chatRoom.setName(name);
        chatRoom = chatRoomRepository.save(chatRoom);
        return ChatRoomDto.fromEntity(chatRoom);
    }

    @Override
    public ChatRoomDto deleteChatRoom(String name) {
        if(Strings.isNullOrEmpty(name)) {
            throw new IllegalArgumentException("Name is empty or null");
        }
        try {
            chatRoomRepository.deleteById(name);
        } catch (EmptyResultDataAccessException e) {
            throw new NoSuchElementException("No chat room with name: " + name);
        }
        return new ChatRoomDto(name, null);
    }

    @Override
    public PageDto<ChatRoomDto> listChatRoom(int page, int limit) {
        Page<ChatRoom> result = chatRoomRepository.findAll(PageRequest.of(page, limit));
        return new PageDto<>(ChatRoomDto.fromEntity(result.getContent()), result.getPageable(), result.getTotalElements());
    }
}
