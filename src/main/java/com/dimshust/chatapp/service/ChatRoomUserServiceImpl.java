package com.dimshust.chatapp.service;

import com.dimshust.chatapp.dto.ChatRoomUserDto;
import com.dimshust.chatapp.dto.PageDto;
import com.dimshust.chatapp.main.model.ChatRoom;
import com.dimshust.chatapp.main.model.ChatRoomUser;
import com.dimshust.chatapp.main.repository.ChatRoomRepository;
import com.dimshust.chatapp.main.repository.ChatRoomUserRepository;
import com.google.common.base.Strings;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
@Log4j2
public class ChatRoomUserServiceImpl implements ChatRoomUserService {

    private static final String CLIENT_ID_VALIDATION_ERROR = "client id is not present";
    private static final String CHAT_ROOM_NAME_VALIDATION_ERROR = "chat room name is not present";
    private static final String CHAT_ROOM_USER_NAME_VALIDATION_ERROR = "chat room user name is not present";


    private final ChatRoomUserRepository chatRoomUserRepository;
    private final ChatRoomRepository chatRoomRepository;

    public ChatRoomUserServiceImpl(ChatRoomUserRepository chatRoomUserRepository, ChatRoomRepository chatRoomRepository) {
        this.chatRoomRepository = chatRoomRepository;
        this.chatRoomUserRepository = chatRoomUserRepository;
    }

    @Override
    public ChatRoomUserDto registerUserInChatRoom(String clientId, String chatRoomName, String name) {
        if(Strings.isNullOrEmpty(clientId)) {
            throw new IllegalArgumentException(CLIENT_ID_VALIDATION_ERROR);
        }
        if(Strings.isNullOrEmpty(chatRoomName)) {
            throw new IllegalArgumentException(CHAT_ROOM_NAME_VALIDATION_ERROR);
        }
        if(Strings.isNullOrEmpty(name)) {
            throw new IllegalArgumentException(CHAT_ROOM_USER_NAME_VALIDATION_ERROR);
        }

        ChatRoom chatRoom = chatRoomRepository.findById(
                chatRoomName).orElseThrow(() -> new NoSuchElementException("No room with id: " + chatRoomName));

        ChatRoomUser chatRoomUser = chatRoomUserRepository
                .findByClientIdAndChatRoomName(clientId, chatRoomName)
                .orElseGet(() -> createChatRoomUser(clientId, chatRoom, name));

        return ChatRoomUserDto.fromEntity(chatRoomUser);
    }

    @Override
    public ChatRoomUserDto deleteUserFromChatRoom(String clientId, String chatRoomName) {
        if(Strings.isNullOrEmpty(clientId)) {
            throw new IllegalArgumentException(CLIENT_ID_VALIDATION_ERROR);
        }
        if(Strings.isNullOrEmpty(chatRoomName)) {
            throw new IllegalArgumentException(CHAT_ROOM_NAME_VALIDATION_ERROR);
        }
        chatRoomUserRepository.deleteByClientIdAndChatRoomName(clientId, chatRoomName);
        return new ChatRoomUserDto(clientId, chatRoomName, null);
    }

    @Override
    public ChatRoomUserDto deleteUser(String clientId) {
        if(Strings.isNullOrEmpty(clientId)) {
            throw new IllegalArgumentException(CLIENT_ID_VALIDATION_ERROR);
        }
        chatRoomUserRepository.deleteByClientId(clientId);
        return new ChatRoomUserDto(clientId, null, null);
    }

    @Override
    public PageDto<ChatRoomUserDto> getUsersInChatRoom(String chatRoomName, int page, int limit) {
        if(Strings.isNullOrEmpty(chatRoomName)) {
            throw new IllegalArgumentException(CHAT_ROOM_NAME_VALIDATION_ERROR);
        }
        Page<ChatRoomUser> result = chatRoomUserRepository.findByChatRoomName(chatRoomName, PageRequest.of(page, limit));
        return new PageDto<>(ChatRoomUserDto.fromEntity(result.getContent()), result.getPageable(), result.getTotalElements());
    }

    @Override
    public ChatRoomUserDto getUserInChatRoom(String chatRoomName, String clientId) {
        if(Strings.isNullOrEmpty(clientId)) {
            throw new IllegalArgumentException(CLIENT_ID_VALIDATION_ERROR);
        }
        if(Strings.isNullOrEmpty(chatRoomName)) {
            throw new IllegalArgumentException(CHAT_ROOM_NAME_VALIDATION_ERROR);
        }
        return ChatRoomUserDto.fromEntity(chatRoomUserRepository
                .findByClientIdAndChatRoomName(clientId, chatRoomName)
                .orElseThrow(() -> new NoSuchElementException("No user with client id: " + clientId + " and chat room name: " + chatRoomName)));

    }

    private ChatRoomUser createChatRoomUser(String clientId, ChatRoom chatRoom, String name) {
        Optional<ChatRoomUser> findUserWithNameResult = chatRoomUserRepository.findByChatRoomNameAndName(chatRoom.getName(), name);
        ChatRoomUser userEntity = new ChatRoomUser();
        userEntity.setChatRoomName(chatRoom.getName());
        userEntity.setChatRoom(chatRoom);
        userEntity.setClientId(clientId);
        if(findUserWithNameResult.isPresent())
            userEntity.setName(UUID.randomUUID().toString());
        else {
            userEntity.setName(name);
        }
        return chatRoomUserRepository.save(userEntity);
    }
}
