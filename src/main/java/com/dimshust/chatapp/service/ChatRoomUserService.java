package com.dimshust.chatapp.service;

import com.dimshust.chatapp.dto.ChatRoomUserDto;
import com.dimshust.chatapp.dto.PageDto;

public interface ChatRoomUserService {
    ChatRoomUserDto registerUserInChatRoom(String clientId, String chatRoomName, String name);
    ChatRoomUserDto deleteUserFromChatRoom(String clientId, String chatRoomName);
    ChatRoomUserDto deleteUser(String clientId);
    PageDto<ChatRoomUserDto> getUsersInChatRoom(String chatRoomName, int page, int limit);
    ChatRoomUserDto getUserInChatRoom(String chatRoomName, String clientId);

}
