package com.dimshust.chatapp.configuration;

import com.dimshust.chatapp.system.CustomHandshakeHandler;
import com.dimshust.chatapp.system.UserHeaderChannelInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    UserHeaderChannelInterceptor userHeaderChannelInterceptor;

    public WebSocketConfig(UserHeaderChannelInterceptor userHeaderChannelInterceptor) {
        this.userHeaderChannelInterceptor = userHeaderChannelInterceptor;
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/v1/chat").setHandshakeHandler(new CustomHandshakeHandler()).withSockJS();
        registry.addEndpoint("/v1/userinfo").setHandshakeHandler(new CustomHandshakeHandler()).withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config){
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.interceptors(userHeaderChannelInterceptor);
    }
}