package com.dimshust.chatapp.controller;

import com.dimshust.chatapp.dto.ChatRoomUserDto;
import com.dimshust.chatapp.dto.PageDto;
import com.dimshust.chatapp.service.ChatRoomUserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Log4j2
@Controller
@RequestMapping(path="/api/v1/user")
public class ChatRoomUserController {
    private final ChatRoomUserService chatRoomUserService;

    public ChatRoomUserController(ChatRoomUserService chatRoomUserService) {
        this.chatRoomUserService = chatRoomUserService;
    }

    @GetMapping(path = "/list")
    @ResponseBody
    public PageDto<ChatRoomUserDto> getUsersInChatRoom(@RequestParam("chatRoomName") String chatRoomName,
                                                @RequestParam(value = "page", defaultValue = "0") Integer page,
                                                @RequestParam(value = "limit", defaultValue = "100") Integer limit) {
        return chatRoomUserService.getUsersInChatRoom(chatRoomName, page, limit);
    }

    @GetMapping
    @ResponseBody
    public ChatRoomUserDto getUserInChatRoom(@RequestParam("chatRoomName") String chatRoomName,
                                            @RequestParam("clientId") String clientId,
                                            @RequestParam(value = "page", defaultValue = "0") Integer page,
                                            @RequestParam(value = "limit", defaultValue = "100") Integer limit) {
        return chatRoomUserService.getUserInChatRoom(chatRoomName, clientId);
    }

}
