package com.dimshust.chatapp;

import com.dimshust.chatapp.configuration.ApplicationConfig;
import com.dimshust.chatapp.configuration.SwaggerConfig;
import com.dimshust.chatapp.configuration.WebSocketConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({ApplicationConfig.class, WebSocketConfig.class, SwaggerConfig.class})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}