package com.dimshust.chatapp.main.repository;

import com.dimshust.chatapp.main.model.ChatRoomUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChatRoomUserRepository extends PagingAndSortingRepository<ChatRoomUser, String> {
    Optional<ChatRoomUser> findByClientIdAndChatRoomName(String clientId, String chatRoomName);
    Optional<ChatRoomUser> findByChatRoomNameAndName(String chatRoomName, String name);
    Page<ChatRoomUser> findByChatRoomName(String chatRoomName, Pageable pageable);
    void deleteByClientIdAndChatRoomName(String clientId, String chatRoomName);
    void deleteByClientId(String clientId);
}
