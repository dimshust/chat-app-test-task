package com.dimshust.chatapp.main.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(of = {"name"})
@Entity
@Table(name = "chat_room")
public class ChatRoom {

    @Id
    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "chatRoom", fetch = FetchType.LAZY)
    List<ChatRoomUser> users;
}
