package com.dimshust.chatapp.main.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(of = {"clientId", "chatRoomName"})
@Entity
@Table(name = "chat_room_user")
@IdClass(ChatRoomUserId.class)
public class ChatRoomUser {

    @Id
    @Column(name = "CLIENT_ID")
    private String clientId;

    @Id
    @Column(name = "CHAT_ROOM_NAME")
    private String chatRoomName;

    @Column(name = "NAME", unique = true)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CHAT_ROOM_ID", insertable = false, updatable = false)
    ChatRoom chatRoom;

}
